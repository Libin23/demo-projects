import React, {useState} from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import ArrowUp from '../../assets/icons/arrowup.svg';
import Home from '../screens/Home';
import {SIZES} from '../constants/Constants';
import Faq from '../screens/Faq';
import Profile from '../screens/Profile';
import {FONTS} from '../constants/index';
import Meditation from '../screens/Meditation';
import WeeklyGoals from '../screens/WeeklyGoals';
import HomeIcon from '../../assets/icons/home.svg';
import HomeClr from '../../assets/icons/homeclr.svg';
import GoalsIcon from '../../assets/icons/graygoals.svg';
import GoalsClr from '../../assets/icons/goalsclr.svg';
import MedIcon from '../../assets/icons/medgray.svg';
import MedClr from '../../assets/icons/medclr.svg';
import FaIcon from '../../assets/icons/fagray.svg';
import FaClr from '../../assets/icons/faclr.svg';

const Tab = createBottomTabNavigator();

export default function BottomTab() {
  const getTabStyle = focused => {
    return StyleSheet.create({
      tabText: {
        fontFamily: FONTS.nunito_regular,
        fontSize: SIZES.wp('3.5%'),
        marginTop: SIZES.wp('0.4%'),
        color: focused ? '#8587DC' : '#8C8C8C',
      },
    });
  };
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: {
          backgroundColor: '#FFFFFF',
          alignItems: 'center',
          justifyContent: 'center',
          height: SIZES.wp('20%'),
          borderTopWidth: 0,
          paddingHorizontal: SIZES.wp('1.7%'),
        },
        //   tabBarLabelStyle: {fontSize: 0},
        headerShown: false,
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: () => <Text></Text>,
          tabBarIcon: ({focused}) => (
            <View style={styles.itemContainer}>
              {focused ? (
                <HomeClr width="100%" height="100%" />
              ) : (
                <HomeIcon width="100%" height="100%" />
              )}
              <Text style={getTabStyle(focused).tabText}>Home</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="WeeklyGoals"
        component={WeeklyGoals}
        options={{
          tabBarLabel: () => <Text></Text>,
          tabBarIcon: ({focused}) => (
            <View style={styles.itemContainer}>
              {focused ? (
                <GoalsClr width="100%" height="100%" />
              ) : (
                <GoalsIcon width="100%" height="100%" />
              )}
              <Text style={getTabStyle(focused).tabText}>Goals</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Meditation"
        component={Meditation}
        options={{
          tabBarLabel: () => <Text></Text>,
          tabBarIcon: ({focused}) => (
            <View style={styles.itemContainer}>
              {focused ? (
                <MedClr width="100%" height="100%" />
              ) : (
                <MedIcon width="100%" height="100%" />
              )}
              <Text style={getTabStyle(focused).tabText}>Meditation</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Faq"
        component={Faq}
        options={{
          tabBarLabel: () => <Text></Text>,
          tabBarIcon: ({focused}) => (
            <View style={styles.itemContainer}>
              {focused ? (
                <FaClr width="100%" height="100%" />
              ) : (
                <FaIcon width="100%" height="100%" />
              )}
              <Text style={getTabStyle(focused).tabText}>FAQ</Text>
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    top: SIZES.wp('6%'),
    position: 'absolute',
  },
});
