import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../screens/Home';
import Google from '../screens/Google';
import Onboard from '../screens/Onboard';
import Profile from '../screens/Profile';
import DailyGoals from '../includes/home/DailyGoals';
import Meditation from '../screens/Meditation';
import NoInternet from '../includes/home/NoInterNet';
import WeeklyGoals from '../screens/WeeklyGoals';
import Faq from '../screens/Faq';
import BottomTab from './BottomTab';
import MindTracker from '../screens/MindTracker';

const Stack = createNativeStackNavigator();

export default function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={Onboard}
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="Onboard" component={Onboard} />
        <Stack.Screen name="Google" component={Google} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="BottomTab" component={BottomTab} />
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="DailyGoals" component={DailyGoals} />
        <Stack.Screen name="Meditation" component={Meditation} />
        <Stack.Screen name="NoInternet" component={NoInternet} />
        <Stack.Screen name="WeeklyGoals" component={WeeklyGoals} />
        <Stack.Screen name="Faq" component={Faq} />
        <Stack.Screen name="MindTracker" component={MindTracker} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
