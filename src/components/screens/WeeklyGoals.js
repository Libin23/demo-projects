import {
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  View,
} from 'react-native';
import React from 'react';
import {SIZES} from '../constants/Constants';
import {FONTS} from '../constants/index';
import RightArrow from '../../assets/icons/rightarrow';

const WeeklyGoals = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.topDiv}>
        <Text style={styles.profileText}>Weekly Goals</Text>
      </View>
      <ScrollView showsVerticalScrollIndicator={false} style={styles.bottomDiv}>
        <Text style={styles.nameText}>Mindfulness Meditation</Text>
        <View style={styles.DailyContainer}>
          <View style={styles.ButtonCom}>
            <Text style={styles.completeText}>Completed</Text>
          </View>
          <View style={styles.DailyDivLeft}></View>
          <View style={styles.DailyDivRight}>
            <Text style={styles.today}>Self-Care Ritual</Text>
            <View style={styles.DivRightText}>
              <Text style={styles.mind}>Day 2 - for body</Text>
              <TouchableOpacity
                onPress={() => navigation.navigate('DailyGoals')}
                style={styles.DivRightView}>
                <View style={styles.arrowRight}>
                  <RightArrow width="100%" height="100%" />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.DailyContainer}>
          <View style={styles.ButtonCom}>
            <Text style={styles.completeText}>Completed</Text>
          </View>
          <View style={styles.DailyDivLeft}></View>
          <View style={styles.DailyDivRight}>
            <Text style={styles.today}>Self-Care Ritual</Text>
            <View style={styles.DivRightText}>
              <Text style={styles.mind}>Day 2 - for body</Text>
              <TouchableOpacity
                onPress={() => navigation.navigate('DailyGoals')}
                style={styles.DivRightView}>
                <View style={styles.arrowRight}>
                  <RightArrow width="100%" height="100%" />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.DailyContainer}>
          <View style={styles.ButtonCom}>
            <Text style={styles.completeText}>Completed</Text>
          </View>
          <View style={styles.DailyDivLeft}></View>
          <View style={styles.DailyDivRight}>
            <Text style={styles.today}>Self-Care Ritual</Text>
            <View style={styles.DivRightText}>
              <Text style={styles.mind}>Day 2 - for body</Text>
              <TouchableOpacity
                onPress={() => navigation.navigate('DailyGoals')}
                style={styles.DivRightView}>
                <View style={styles.arrowRight}>
                  <RightArrow width="100%" height="100%" />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.DailyContainer}>
          <View style={styles.ButtonCom}>
            <Text style={styles.completeText}>Completed</Text>
          </View>
          <View style={styles.DailyDivLeft}></View>
          <View style={styles.DailyDivRight}>
            <Text style={styles.today}>Self-Care Ritual</Text>
            <View style={styles.DivRightText}>
              <Text style={styles.mind}>Day 2 - for body</Text>
              <TouchableOpacity
                onPress={() => navigation.navigate('DailyGoals')}
                style={styles.DivRightView}>
                <View style={styles.arrowRight}>
                  <RightArrow width="100%" height="100%" />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default WeeklyGoals;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  topDiv: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: SIZES.wp('7%'),
    borderBottomWidth: 1,
    borderColor: '#F5F5F5',
    paddingHorizontal: SIZES.wp('5%'),
  },
  nameText: {
    fontFamily: FONTS.nunito_semibold,
    fontSize: SIZES.wp('4.3%'),
    color: '#000000',
    marginVertical: SIZES.wp('5%'),
  },
  profileText: {
    fontFamily: FONTS.nunito_semibold,
    fontSize: SIZES.wp('4.5%'),
    color: '#000000',
  },
  bottomDiv: {
    flex: 1,
    paddingHorizontal: SIZES.wp('5%'),
  },
  DailyContainer: {
    borderWidth: 1,
    borderColor: '#F8F2FF',
    padding: SIZES.wp('4%'),
    marginBottom: SIZES.wp('3%'),
    borderRadius: SIZES.wp('3.5%'),
    flexDirection: 'row',
  },
  ButtonCom: {
    backgroundColor: '#C8F1C4',
    position: 'absolute',
    top: 0,
    right: 0,
    paddingVertical: SIZES.wp('1%'),
    paddingHorizontal: SIZES.wp('3%'),
    borderBottomLeftRadius: SIZES.wp('2%'),
    borderTopRightRadius: SIZES.wp('2%'),
  },
  completeText: {
    fontFamily: FONTS.nunito_regular,
    color: '#6CAE67',
    fontSize: SIZES.wp('3.4%'),
  },
  DailyDivLeft: {
    borderLeftWidth: 1,
    marginTop: SIZES.wp('2%'),
  },
  DailyDivRight: {
    justifyContent: 'center',
  },
  DivRightText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: SIZES.wp('3%'),
    marginTop: SIZES.wp('3%'),
    width: SIZES.wp('78%'),
  },
  DivRightView: {
    flexDirection: 'row',
    marginLeft: SIZES.wp('18%'),
    alignItems: 'center',
  },
  arrowRight: {
    width: SIZES.wp('4.3%'),
    height: SIZES.wp('4.3%'),
    marginLeft: SIZES.wp('2%'),
  },
  mind: {
    fontFamily: FONTS.nunito_regular,
    color: '#BCBBBC',
    fontSize: SIZES.wp('3.8%'),
  },
  scrollContain: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  emojyContain: {
    alignItems: 'center',
  },
  emojyDiv: {
    width: SIZES.wp('19%'),
    height: SIZES.wp('13%'),
    marginBottom: SIZES.wp('1.5%'),
    borderWidth: 1,
    borderRadius: SIZES.wp('2%'),
  },

  feelText: {
    fontFamily: FONTS.nunito_semibold,
    color: '#7E77DD',
    fontSize: SIZES.wp('3.5%'),
  },
  today: {
    fontFamily: FONTS.nunito_semibold,
    color: '#000000',
    fontSize: SIZES.wp('4.3%'),
    marginLeft: SIZES.wp('3%'),
  },
});
