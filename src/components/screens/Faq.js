import {
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  View,
  Animated,
  LayoutAnimation,
  Easing,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {SIZES} from '../constants/Constants';
import {FONTS} from '../constants/index';
import ArrowUp from '../../assets/icons/arrowup.svg';

const Faq = ({navigation}) => {
  const [visibleItemIndices, setVisibleItemIndices] = useState([]);
  const [isVisible, setIsvisible] = useState(false);
  //   const [menuItem, setMenuItem] = useState([]);
  const [duaItem, setDuas] = useState([]);
  const [rotationValues, setRotationValues] = useState([]);
  const [duaindex, setDuaindex] = useState();

  const menuItem = [
    {
      id: '1',
      title: 'My Account Been Disabled.What Can I Do ?',
      duas: [
        {
          id: 'd1',

          english:
            'Lorem ipsum dolor sit amet consectetur. Enim pharetra quisque maecenas sed imperdiet amet viverra. Lacinia a amet diam volutpat et egestas erat nullam tellus. Vulputate vitae mattis eu cursus congue massa. Amet platea integer arcu ut dolor posuere convallis diam.',
        },
      ],
    },
    {
      id: '2',
      title: 'My Account Been Disabled.What Can I Do ?',
      duas: [
        {
          id: 'd1',

          english:
            'Lorem ipsum dolor sit amet consectetur. Enim pharetra quisque maecenas sed imperdiet amet viverra. Lacinia a amet diam volutpat et egestas erat nullam tellus. Vulputate vitae mattis eu cursus congue massa. Amet platea integer arcu ut dolor posuere convallis diam.',
        },
      ],
    },
    {
      id: '3',
      title: 'My Account Been Disabled.What Can I Do ?',
      duas: [
        {
          id: 'd1',

          english:
            'Lorem ipsum dolor sit amet consectetur. Enim pharetra quisque maecenas sed imperdiet amet viverra. Lacinia a amet diam volutpat et egestas erat nullam tellus. Vulputate vitae mattis eu cursus congue massa. Amet platea integer arcu ut dolor posuere convallis diam.',
        },
      ],
    },
    {
      id: '4',
      title: 'My Account Been Disabled.What Can I Do ?',
      duas: [
        {
          id: 'd1',

          english:
            'Lorem ipsum dolor sit amet consectetur. Enim pharetra quisque maecenas sed imperdiet amet viverra. Lacinia a amet diam volutpat et egestas erat nullam tellus. Vulputate vitae mattis eu cursus congue massa. Amet platea integer arcu ut dolor posuere convallis diam.',
        },
      ],
    },
    {
      id: '5',
      title: 'My Account Been Disabled.What Can I Do ?',
      duas: [
        {
          id: 'd1',

          english:
            'Lorem ipsum dolor sit amet consectetur. Enim pharetra quisque maecenas sed imperdiet amet viverra. Lacinia a amet diam volutpat et egestas erat nullam tellus. Vulputate vitae mattis eu cursus congue massa. Amet platea integer arcu ut dolor posuere convallis diam.',
        },
      ],
    },
    {
      id: '6',
      title: 'My Account Been Disabled.What Can I Do ?',
      duas: [
        {
          id: 'd1',

          english:
            'Lorem ipsum dolor sit amet consectetur. Enim pharetra quisque maecenas sed imperdiet amet viverra. Lacinia a amet diam volutpat et egestas erat nullam tellus. Vulputate vitae mattis eu cursus congue massa. Amet platea integer arcu ut dolor posuere convallis diam.',
        },
      ],
    },
    {
      id: '7',
      title: 'My Account Been Disabled.What Can I Do ?',
      duas: [
        {
          id: 'd1',

          english:
            'Lorem ipsum dolor sit amet consectetur. Enim pharetra quisque maecenas sed imperdiet amet viverra. Lacinia a amet diam volutpat et egestas erat nullam tellus. Vulputate vitae mattis eu cursus congue massa. Amet platea integer arcu ut dolor posuere convallis diam.',
        },
      ],
    },
    {
      id: '8',
      title: 'My Account Been Disabled.What Can I Do ?',
      duas: [
        {
          id: 'd1',

          english:
            'Lorem ipsum dolor sit amet consectetur. Enim pharetra quisque maecenas sed imperdiet amet viverra. Lacinia a amet diam volutpat et egestas erat nullam tellus. Vulputate vitae mattis eu cursus congue massa. Amet platea integer arcu ut dolor posuere convallis diam.',
        },
      ],
    },
  ];
  function Rotate(index) {
    if (rotationValues[index]) {
      const arrowTransform = rotationValues[index].interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '180deg'],
      });
      // console.log('Arrow Transform:', arrowTransform);
      return {transform: [{rotate: arrowTransform}]};
    } else {
      return null;
    }
  }

  const toggleListItem = index => {
    const newRotationValues = [...rotationValues];
    const newVisibleItemIndices = [...visibleItemIndices];

    if (newVisibleItemIndices.includes(index)) {
      // Close the clicked item
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      newRotationValues[index] = new Animated.Value(0); // Reset rotation
      Animated.timing(newRotationValues[index], {
        toValue: 1,
        duration: 300,
        easing: Easing.linear,
        useNativeDriver: true,
      }).start();

      // Remove the item from the visibleItemIndices
      const indexToRemove = newVisibleItemIndices.indexOf(index);
      newVisibleItemIndices.splice(indexToRemove, 1);
    } else {
      // Close the previously opened item
      if (newVisibleItemIndices.length > 0) {
        const previousIndex = newVisibleItemIndices[0];
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        newRotationValues[previousIndex] = new Animated.Value(1); // Reset rotation
        Animated.timing(newRotationValues[previousIndex], {
          toValue: 1,
          duration: 300,
          easing: Easing.linear,
          useNativeDriver: true,
        }).start();

        // Remove the previously opened item from the visibleItemIndices
        newVisibleItemIndices.pop();
      }

      // Open the clicked item
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      newRotationValues[index] = new Animated.Value(1); // Reset rotation
      Animated.timing(newRotationValues[index], {
        toValue: 0,
        duration: 300,
        easing: Easing.linear,
        useNativeDriver: true,
      }).start();

      // Add the clicked item to the visibleItemIndices
      newVisibleItemIndices.push(index);
    }

    setIsvisible(!isVisible);
    setRotationValues(newRotationValues);
    setVisibleItemIndices(newVisibleItemIndices);
  };
  return (
    <View style={styles.container}>
      <View style={styles.topDiv}>
        <Text style={styles.profileText}>FAQ</Text>
      </View>
      <ScrollView showsVerticalScrollIndicator={false} style={styles.bottomDiv}>
        <Text style={styles.nameText}>Frequently Asked{'\n'}Questions </Text>
        {menuItem.map((item, index) => (
          <TouchableOpacity
            key={index}
            activeOpacity={0.8}
            onPress={() => {
              toggleListItem(index, item.id);
              setDuaindex(duaindex === index ? '' : index);
            }}
            style={[
              styles.contentContainer,
              {
                backgroundColor: duaindex === index ? '#FAFAFF' : '#FCFCFC',
              },
            ]}>
            <View style={styles.subcontentContainer}>
              <View style={styles.leftDiv}>
                <Text
                  style={[
                    styles.contentText,
                    {
                      color: '#000000',
                    },
                  ]}>
                  <Text style={styles.countText}>
                    {index + 1 < 10 && '0'}
                    {index + 1} .{' '}
                  </Text>
                  {item.title}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.arrowButton}
                activeOpacity={0.8}
                onPress={() => {
                  toggleListItem(index, item.id);
                  setDuaindex(duaindex === index ? '' : index);
                }}>
                <Animated.View style={Rotate(index)}>
                  <ArrowUp width={'100%'} height={'100%'} />
                </Animated.View>
              </TouchableOpacity>
            </View>
            {duaindex === index &&
              item.duas.map(item => (
                <View style={styles.subContent} key={item.id}>
                  <Text style={styles.subText}>{item.english}</Text>
                </View>
              ))}
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

export default Faq;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  topDiv: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: SIZES.wp('7%'),
    borderBottomWidth: 1,
    borderColor: '#F5F5F5',
    paddingHorizontal: SIZES.wp('5%'),
  },
  nameText: {
    fontFamily: FONTS.nunito_semibold,
    fontSize: SIZES.wp('4.8%'),
    color: '#000000',
    marginVertical: SIZES.wp('5%'),
  },
  profileText: {
    fontFamily: FONTS.nunito_semibold,
    fontSize: SIZES.wp('4.7%'),
    color: '#000000',
  },
  bottomDiv: {
    flex: 1,
    paddingHorizontal: SIZES.wp('5%'),
  },

  // ---------->
  contentContainer: {
    marginBottom: SIZES.wp('3.5%'),
    borderRadius: SIZES.wp('4%'),
    overflow: 'hidden',
    borderWidth: SIZES.wp('0.1%'),
    borderColor: '#D9D9D9',
  },
  subcontentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: SIZES.wp('4%'),
    paddingRight: SIZES.wp('5%'),
    paddingVertical: SIZES.wp('5%'),
  },
  leftDiv: {
    flexDirection: 'row',
  },

  contentText: {
    fontFamily: FONTS.nunito_semibold,
    fontSize: SIZES.wp('4.5%'),
    width: SIZES.wp('72%'),
  },
  arrowButton: {
    width: SIZES.wp('4.5%'),
    height: SIZES.wp('4.5%'),
    marginLeft: SIZES.wp('1%'),
  },
  countText: {
    fontFamily: FONTS.nunito_bold,
    fontSize: SIZES.wp('4%'),
    color: '#000000',
  },
  subContent: {
    marginBottom: SIZES.wp('5.5%'),
    paddingLeft: SIZES.wp('4%'),
  },
  subText: {
    color: '#5E5E5E',
    fontFamily: FONTS.nunito_regular,
    fontSize: SIZES.wp('3.7%'),
    width: '95%',
  },
});
