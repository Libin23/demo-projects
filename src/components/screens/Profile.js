import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {SIZES} from '../constants/Constants';
import {FONTS} from '../constants/index';
import LeftArrow from '../../assets/icons/leftarrow.svg';
import Dlt from '../../assets/icons/dlt.svg';
import Exit from '../../assets/icons/exit.svg';

const Profile = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.topDiv}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.leftArrowDiv}>
          <LeftArrow width="100%" height="100%" />
        </TouchableOpacity>
        <Text style={styles.profileText}>Profile</Text>
        <View style={styles.leftArrowDiv}></View>
      </View>
      <View style={styles.midDiv}>
        <View style={styles.imageDiv}></View>
        <Text style={styles.nameText}>Lasfa</Text>
        <Text style={styles.mailText}>lasfa@gmail.com</Text>
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate('NoInternet')}
        style={styles.DltContain}>
        <View style={styles.DltIconDiv}>
          <Dlt width="100%" height="100%" />
        </View>
        <Text style={styles.dltText}>Delete account</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.DltContain}>
        <View style={styles.DltIconDiv}>
          <Exit width="100%" height="100%" />
        </View>
        <Text style={styles.dltText}>Logout</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  topDiv: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: SIZES.wp('7%'),
    borderBottomWidth: 1,
    borderColor: '#F5F5F5',
    paddingHorizontal: SIZES.wp('5%'),
  },
  leftArrowDiv: {
    width: SIZES.wp('7%'),
    height: SIZES.wp('7%'),
  },
  profileText: {
    fontFamily: FONTS.nunito_semibold,
    fontSize: SIZES.wp('4.7%'),
    color: '#000000',
  },
  midDiv: {
    alignItems: 'center',
    marginTop: SIZES.wp('25%'),
    marginBottom: SIZES.wp('15%'),
  },
  imageDiv: {
    width: SIZES.wp('25%'),
    height: SIZES.wp('25%'),
    borderWidth: 1,
    borderRadius: SIZES.wp('13%'),
  },
  nameText: {
    marginTop: SIZES.wp('3%'),
    fontFamily: FONTS.nunito_semibold,
    fontSize: SIZES.wp('4.7%'),
    color: '#000000',
  },
  mailText: {
    marginTop: SIZES.wp('0.5%'),
    fontFamily: FONTS.nunito_regular,
    fontSize: SIZES.wp('3.6%'),
    color: '#000000',
  },
  DltContain: {
    paddingHorizontal: SIZES.wp('5%'),
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: SIZES.wp('4%'),
  },
  DltIconDiv: {
    width: SIZES.wp('10%'),
    height: SIZES.wp('10%'),
    padding: SIZES.wp('2%'),
    // borderWidth: 1,
    borderRadius: SIZES.wp('13%'),
    backgroundColor: '#F4F4FF',
    marginRight: SIZES.wp('2%'),
  },
  dltText: {
    fontFamily: FONTS.nunito_medium,
    fontSize: SIZES.wp('4.3%'),
    color: '#3E3E3E',
  },
});
