import {StyleSheet, Text, TouchableOpacity, Image, View} from 'react-native';
import React from 'react';
import {SIZES} from '../constants/Constants';
import {FONTS} from '../constants/index';

const Onboard = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.topDiv}>
        <View style={styles.imageDiv}>
          <Image
            style={styles.image}
            source={require('../../assets/images/onboard/girl.png')}
          />
        </View>
      </View>
      <View style={styles.bottomDiv}>
        <Text style={styles.welcome}>Welcome to your safe space</Text>
        <Text style={styles.pText}>
          Lorem ipsum dolor sit amet consectetur. Nisi donec nunc nascetur sed
          dolor proin quisque. Diam ut feugiat ornare blandit
        </Text>
        <TouchableOpacity
          onPress={() => navigation.navigate('Google')}
          style={styles.button}>
          <Text style={styles.nextText}>Next</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Onboard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: SIZES.wp('5%'),
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingBottom: SIZES.wp('5%'),
  },
  topDiv: {
    paddingTop: SIZES.wp('25%'),
  },
  imageDiv: {
    width: SIZES.wp('60%'),
    height: SIZES.wp('60%'),
  },
  image: {
    width: '100%',
    height: '100%',
  },
  bottomDiv: {
    borderWidth: 1,
    paddingHorizontal: SIZES.wp('10%'),
    backgroundColor: '#FAFAFF',
    paddingVertical: SIZES.wp('5%'),
    alignItems: 'center',
    borderColor: '#EFEFFF',
    borderRadius: SIZES.wp('7%'),
  },
  button: {
    width: SIZES.wp('30%'),
    backgroundColor: '#8587DC',
    paddingVertical: SIZES.wp('3%'),
    borderRadius: SIZES.wp('5%'),
    alignItems: 'center',
  },
  welcome: {
    fontFamily: FONTS.nunito_bold,
    color: '#070707',
    fontSize: SIZES.wp('4.5%'),
    marginBottom: SIZES.wp('3.5%'),
  },
  pText: {
    fontFamily: FONTS.nunito_regular,
    color: '#070707',
    fontSize: SIZES.wp('3.3%'),
    textAlign: 'center',
    marginBottom: SIZES.wp('10%'),
  },
  nextText: {
    fontFamily: FONTS.nunito_medium,
    color: '#FDFBFF',
    fontSize: SIZES.wp('3.5%'),
  },
});
