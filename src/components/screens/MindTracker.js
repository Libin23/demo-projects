import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {SIZES} from '../constants/Constants';
import {FONTS} from '../constants/index';

const MindTracker = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.topContain}>
        <Text style={styles.moodText}>Mood & Mind Tracker</Text>
      </View>
      <View style={styles.progress}></View>
      <View style={styles.assesmentCard}>
        <View style={styles.positionNumber}>
          <Text style={styles.nextText}>1</Text>
        </View>
        <Text style={styles.moodText}>Mood & Mind Tracker</Text>
        <View style={styles.buttonContain}>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.ansrText}>Yes, quite a lot </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.ansrText}>Yes, quite a lot </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.ansrText}>Yes, quite a lot </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.ansrText}>Yes, quite a lot </Text>
          </TouchableOpacity>
        </View>
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate('BottomTab')}
        style={styles.clrButton}>
        <Text style={styles.nextText}>Next</Text>
      </TouchableOpacity>
    </View>
  );
};

export default MindTracker;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  topContain: {
    paddingTop: SIZES.wp('10%'),
    paddingHorizontal: SIZES.wp('5%'),
  },
  moodText: {
    fontFamily: FONTS.nunito_semibold,
    color: '#070707',
    fontSize: SIZES.wp('5%'),
  },
  progress: {
    width: SIZES.wp('90%'),
    padding: SIZES.wp('0.7%'),
    backgroundColor: '#8587DC',
    borderRadius: SIZES.wp('7%'),
    marginTop: SIZES.wp('5%'),
    marginBottom: SIZES.wp('10%'),
    marginHorizontal: SIZES.wp('5%'),
  },
  assesmentCard: {
    backgroundColor: '#FDFBFF',
    paddingHorizontal: SIZES.wp('5%'),
    paddingTop: SIZES.wp('10%'),
    paddingBottom: SIZES.wp('3%'),
    borderRadius: SIZES.wp('6%'),
    borderWidth: 1,
    borderColor: '#FAF5FF',
  },
  positionNumber: {
    backgroundColor: '#C5C1FF',
    height: SIZES.wp('8%'),
    width: SIZES.wp('8%'),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: SIZES.wp('2%'),
    position: 'absolute',
    top: 0,
    left: SIZES.wp('9%'),
  },
  buttonContain: {
    marginTop: SIZES.wp('5%'),
  },
  button: {
    width: SIZES.wp('90%'),
    backgroundColor: '#FFFFFF',
    paddingHorizontal: SIZES.wp('5%'),
    paddingVertical: SIZES.wp('5%'),
    borderRadius: SIZES.wp('10%'),
    borderColor: '#EFEFFF',
    borderWidth: 1,
    marginBottom: SIZES.wp('3%'),
  },
  ansrText: {
    fontFamily: FONTS.nunito_medium,
    color: '#626262',
    fontSize: SIZES.wp('3.5%'),
  },
  nextText: {
    fontFamily: FONTS.nunito_medium,
    color: '#FDFBFF',
    fontSize: SIZES.wp('3.8%'),
  },
  clrButton: {
    width: SIZES.wp('90%'),
    backgroundColor: '#8587DC',
    paddingHorizontal: SIZES.wp('5%'),
    paddingVertical: SIZES.wp('5%'),
    borderRadius: SIZES.wp('10%'),
    alignSelf: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: SIZES.wp('5%'),
  },
});
