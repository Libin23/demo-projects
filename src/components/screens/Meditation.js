import {
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  View,
} from 'react-native';
import React from 'react';
import {SIZES} from '../constants/Constants';
import {FONTS} from '../constants/index';

const Meditation = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.topDiv}>
        <Text style={styles.profileText}>meditations</Text>
      </View>
      <ScrollView showsVerticalScrollIndicator={false} style={styles.bottomDiv}>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          style={styles.scrollDiv}>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.nextText}>Mindfulness</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.nextText}>Concentration</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.nextText}>Movement </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.nextText}>Mark as completed</Text>
          </TouchableOpacity>
        </ScrollView>
        <View style={styles.midDiv}>
          <View style={styles.dateDiv}>
            {/* <View style={styles.iconDiv}></View> */}
            <Text style={styles.nameText}>Mindfulness Meditation</Text>
          </View>
          <View style={styles.mintDiv}>
            <View style={styles.dotDiv}></View>
            <Text style={styles.mintText}>30 Min - For body</Text>
          </View>
          <View style={styles.ImageContain}>
            <Image
              style={styles.image}
              source={require('../../assets/images/onboard/girl.png')}
            />
          </View>
          <Text style={styles.dailyText}>
            Based on being mindful, or having an increased awareness and
            acceptance of living in the present moment. This involves breathing
            methods, guided imagery, and other practices to relax the body and
            mind and help reduce stress.
            {'\n'}
            {'\n'}
            or having an increased awareness and acceptance of living in the
            present moment. This involves breathing methods, guided imagery, and
            other practices to relax the body and mind and help reduce stress.
            {'\n'}
            {'\n'}
            mindful, or having an increased awareness and acceptance of living
            in the present moment. This involves breathing methods, guided
            imagery, and other practices to relax the body and mind and help
            reduce stress.
            {'\n'}
            {'\n'}
            or having an increased awareness and acceptance of living in the
            present moment. This involves breathing methods, guided imagery, and
            other practices to relax the body and mind and help reduce stress.
            {'\n'}
            {'\n'}
            mindful, or having an increased awareness and acceptance of living
            in the present moment. This involves breathing methods, guided
            imagery, and other practices to relax the body and mind and help
            reduce stress.
          </Text>
        </View>
      </ScrollView>
    </View>
  );
};

export default Meditation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  topDiv: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: SIZES.wp('7%'),
    borderBottomWidth: 1,
    borderColor: '#F5F5F5',
    paddingHorizontal: SIZES.wp('5%'),
  },

  profileText: {
    fontFamily: FONTS.nunito_semibold,
    fontSize: SIZES.wp('4.7%'),
    color: '#000000',
  },
  bottomDiv: {
    flex: 1,
    backgroundColor: '#F6F6FF',
  },
  scrollDiv: {
    paddingVertical: SIZES.wp('5%'),

    marginLeft: SIZES.wp('5%'),
  },
  midDiv: {
    borderWidth: 1,
    borderColor: '#EFF1FF',
    borderRadius: SIZES.wp('4%'),
    paddingHorizontal: SIZES.wp('5%'),
    paddingVertical: SIZES.wp('7%'),
    backgroundColor: '#FFFFFF',
    marginHorizontal: SIZES.wp('5%'),
  },
  dateDiv: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dotDiv: {
    width: SIZES.wp('1.5%'),
    height: SIZES.wp('1.5%'),
    backgroundColor: '#ADA9A2',
    borderRadius: SIZES.wp('13%'),
    marginRight: SIZES.wp('1%'),
  },
  mintText: {
    fontFamily: FONTS.nunito_regular,
    fontSize: SIZES.wp('3.5%'),
    color: '#ADA9A2',
  },
  ImageContain: {
    width: SIZES.wp('35%'),
    height: SIZES.wp('35%'),
    marginBottom: SIZES.wp('4%'),
    alignSelf: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
  },
  dailyText: {
    fontFamily: FONTS.nunito_regular,
    fontSize: SIZES.wp('3.7%'),
    color: '#000000',
  },
  mintDiv: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: SIZES.wp('2%'),
    marginBottom: SIZES.wp('4%'),
  },
  nameText: {
    fontFamily: FONTS.nunito_semibold,
    fontSize: SIZES.wp('4.3%'),
    color: '#000000',
  },
  button: {
    backgroundColor: '#FFFFFF',
    // height: SIZES.wp('10%'),
    paddingVertical: SIZES.wp('3%'),
    paddingHorizontal: SIZES.wp('2%'),
    borderRadius: SIZES.wp('2.5%'),
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: SIZES.wp('3%'),
  },
  nextText: {
    fontFamily: FONTS.nunito_medium,
    color: '#737374',
    fontSize: SIZES.wp('3.8%'),
  },
});
