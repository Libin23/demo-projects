import {StyleSheet, Text, TouchableOpacity, Image, View} from 'react-native';
import React from 'react';
import {SIZES} from '../constants/Constants';
import {FONTS} from '../constants/index';
import GoogleIcon from '../../assets/icons/google.svg';

const Google = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.topDiv}>
        <Text style={styles.welcome}>Hi welcome!</Text>
        <Text style={styles.pText}>
          Lorem ipsum dolor sit amet consectetur. Euismod sagittis egestas
          molestie praesent ut
        </Text>
        <TouchableOpacity style={styles.googleButton}>
          <TouchableOpacity style={styles.iconDiv}>
            <GoogleIcon width={'100%'} height={'100%'} />
          </TouchableOpacity>
          <Text style={styles.conGoogle}>Continue with Google</Text>
        </TouchableOpacity>
        <Text style={styles.termsText}>
          By signing up, you agree to our{' '}
          <Text style={styles.BlueText}>Terms{'\n'},privacy </Text>policy and{' '}
          <Text style={styles.BlueText}>cookie use.</Text>
        </Text>
      </View>
      <View style={styles.bottomDiv}>
        <TouchableOpacity
          onPress={() => navigation.navigate('MindTracker')}
          style={styles.button}>
          <Text style={styles.nextText}>Next</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Google;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: SIZES.wp('5%'),
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingBottom: SIZES.wp('5%'),
  },
  topDiv: {
    paddingTop: SIZES.wp('45%'),
    alignItems: 'center',
  },
  imageDiv: {
    width: SIZES.wp('60%'),
    height: SIZES.wp('60%'),
  },
  image: {
    width: '100%',
    height: '100%',
  },
  bottomDiv: {},
  googleButton: {
    width: SIZES.wp('70%'),
    borderWidth: 1,
    borderColor: '#E7D6FD',
    paddingVertical: SIZES.wp('3%'),
    borderRadius: SIZES.wp('8%'),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  iconDiv: {
    width: SIZES.wp('8%'),
    height: SIZES.wp('8%'),
    marginRight: SIZES.wp('3%'),
  },
  conGoogle: {
    fontFamily: FONTS.nunito_bold,
    color: '#423F3F',
    fontSize: SIZES.wp('4%'),
  },
  button: {
    width: SIZES.wp('90%'),
    backgroundColor: '#8587DC',
    paddingVertical: SIZES.wp('4%'),
    borderRadius: SIZES.wp('8%'),
    alignItems: 'center',
  },
  welcome: {
    fontFamily: FONTS.nunito_bold,
    color: '#070707',
    fontSize: SIZES.wp('5%'),
    marginBottom: SIZES.wp('3.5%'),
    textAlign: 'center',
  },
  termsText: {
    fontFamily: FONTS.nunito_regular,
    color: '#070707',
    fontSize: SIZES.wp('3.5%'),
    textAlign: 'center',
    marginTop: SIZES.wp('5%'),
    width: SIZES.wp('71%'),
  },
  BlueText: {
    color: '#38A0FF',
    fontFamily: FONTS.nunito_semibold,
    fontSize: SIZES.wp('3.5%'),
  },
  pText: {
    fontFamily: FONTS.nunito_regular,
    color: '#070707',
    fontSize: SIZES.wp('3.3%'),
    textAlign: 'center',
    marginBottom: SIZES.wp('25%'),
    width: SIZES.wp('75%'),
  },
  nextText: {
    fontFamily: FONTS.nunito_medium,
    color: '#FDFBFF',
    fontSize: SIZES.wp('3.5%'),
  },
});
