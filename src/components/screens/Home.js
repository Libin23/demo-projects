import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {FONTS} from '../constants/index';
import {SIZES} from '../constants/Constants';
import DailyEmojy from '../includes/home/DailyEmojy';
import WeekCard from '../includes/home/WeekCard';
import Progress from '../../assets/icons/progress.svg';

export default function Home({navigation}) {
  return (
    <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
      <View style={styles.topContainer}>
        <View style={styles.profileLeft}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Profile')}
            style={styles.profileImage}>
            <View style={styles.profilePhotos}></View>
          </TouchableOpacity>
          <View>
            <Text style={styles.profileName}>Hi, afsal</Text>
            <Text style={styles.timeText}>Let’s make this day productive</Text>
          </View>
        </View>
        <TouchableOpacity
          style={styles.round}
          onPress={() => navigation.navigate('Faq')}>
          <View style={styles.moreOptionButton}>
            <Progress width="100%" height="100%" />
          </View>
        </TouchableOpacity>
      </View>
      <ScrollView style={styles.bottomContainer}>
        <DailyEmojy />
        <WeekCard navigation={navigation} />
      </ScrollView>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAF5FF',
  },
  titleText: {
    fontFamily: FONTS.nunito_semibold,
    color: '#5F4D2F',
    fontSize: SIZES.wp('4.7%'),
  },
  round: {
    backgroundColor: '#FFFFFF',
    padding: SIZES.wp('2%'),
    borderRadius: SIZES.wp('7%'),
  },
  moreOptionButton: {
    width: SIZES.wp('6%'),
    height: SIZES.wp('6%'),
  },
  topContainer: {
    justifyContent: 'space-between',
    paddingHorizontal: SIZES.wp('5%'),
    paddingTop: SIZES.wp('5%'),
    paddingBottom: SIZES.wp('8%'),
    alignItems: 'center',
    flexDirection: 'row',
  },
  bottomContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    borderTopLeftRadius: SIZES.wp('8%'),
    borderTopRightRadius: SIZES.wp('8%'),
    paddingBottom: SIZES.wp('4.4%'),
    paddingTop: SIZES.wp('6%'),
  },

  profileLeft: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  profileImage: {
    width: SIZES.wp('11.5%'),
    height: SIZES.wp('11.5%'),
    borderRadius: SIZES.wp('5%'),
    marginRight: SIZES.wp('2%'),
    overflow: 'hidden',
    borderWidth: 1,
  },
  profilePhotos: {
    width: '100%',
    height: '100%',
  },
  profileName: {
    fontFamily: FONTS.nunito_bold,
    fontSize: SIZES.wp('4.3%'),
    color: '#070707',
    marginBottom: SIZES.wp('0.5%'),
  },
  timeText: {
    fontFamily: FONTS.nunito_regular,
    color: '#8A8A8A',
    fontSize: SIZES.wp('3.2%'),
  },
});
