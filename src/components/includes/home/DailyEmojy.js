import {
  StyleSheet,
  Text,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {FONTS} from '../../constants/index';
import {SIZES} from '../../constants/Constants';

const DailyEmojy = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.today}>How are you feeling today ?</Text>
      <ScrollView
        bounces={false}
        horizontal
        showsHorizontalScrollIndicator={false}
        style={styles.scrollContain}>
        <View style={styles.emojyContain}>
          <View style={styles.emojyDiv}></View>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.feelText}>Happy</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.emojyContain}>
          <View style={styles.emojyDiv}></View>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.feelText}>Happy</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.emojyContain}>
          <View style={styles.emojyDiv}></View>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.feelText}>Happy</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.emojyContain}>
          <View style={styles.emojyDiv}></View>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.feelText}>Happy</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.emojyContain}>
          <View style={styles.emojyDiv}></View>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.feelText}>Happy</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.emojyContain}>
          <View style={styles.emojyDiv}></View>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.feelText}>Happy</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default DailyEmojy;

const styles = StyleSheet.create({
  container: {
    paddingLeft: SIZES.wp('5%'),
    paddingBottom: SIZES.wp('5%'),
    borderBottomWidth: 1,
    borderColor: '#F4F4F4',
  },
  today: {
    fontFamily: FONTS.nunito_bold,
    color: '#070707',
    fontSize: SIZES.wp('5%'),
    marginBottom: SIZES.wp('4%'),
  },
  scrollContain: {
    flexDirection: 'row',
  },
  emojyContain: {
    alignItems: 'center',
    marginRight: SIZES.wp('3.5%'),
  },
  emojyDiv: {
    width: SIZES.wp('15%'),
    height: SIZES.wp('15%'),
    marginBottom: SIZES.wp('3.5%'),
    borderWidth: 1,
  },
  button: {
    paddingHorizontal: SIZES.wp('6%'),
    paddingVertical: SIZES.wp('1.7%'),
    borderRadius: SIZES.wp('5%'),
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#EDE7F5',
  },
  feelText: {
    fontFamily: FONTS.nunito_medium,
    color: '#070707',
    fontSize: SIZES.wp('3.7%'),
  },
});
