import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {SIZES} from '../../constants/Constants';
import {FONTS} from '../../constants/index';
import LeftArrow from '../../../assets/icons/leftarrow.svg';
import Calender from '../../../assets/icons/calender.svg';

const DailyGoals = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.topDiv}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.leftArrowDiv}>
          <LeftArrow width="100%" height="100%" />
        </TouchableOpacity>
        <Text style={styles.profileText}>Daily Goals</Text>
        <View style={styles.leftArrowDiv}></View>
      </View>
      <View style={styles.bottomDiv}>
        <View style={styles.midDiv}>
          <View style={styles.dateDiv}>
            <View style={styles.iconDiv}>
              <Calender width="100%" height="100%" />
            </View>
            <Text style={styles.nameText}>Day - 4 : Nutritious Meals</Text>
          </View>
          <View style={styles.mintDiv}>
            <View style={styles.dotDiv}></View>
            <Text style={styles.mintText}>30 Min - For body</Text>
          </View>

          <Text style={styles.dailyText}>
            Lorem ipsum dolor sit amet consectetur. Enim pharetra quisque
            maecenas sed imperdiet amet viverra. Lacinia a amet diam volutpat et
            egestas erat nullam tellus. Vulputate vitae mattis eu cursus congue
            massa. Amet platea integer arcu ut dolor posuere convallis diam.
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('Google')}
          style={styles.button}>
          <Text style={styles.nextText}>Mark as completed</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default DailyGoals;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  topDiv: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: SIZES.wp('7%'),
    borderBottomWidth: 1,
    borderColor: '#F5F5F5',
    paddingHorizontal: SIZES.wp('5%'),
  },
  leftArrowDiv: {
    width: SIZES.wp('7%'),
    height: SIZES.wp('7%'),
  },
  profileText: {
    fontFamily: FONTS.nunito_semibold,
    fontSize: SIZES.wp('4.7%'),
    color: '#000000',
  },
  bottomDiv: {
    paddingHorizontal: SIZES.wp('5%'),
    flex: 1,
  },
  midDiv: {
    marginTop: SIZES.wp('7%'),
    borderWidth: 1,
    borderColor: '#F2E7FF',
    borderRadius: SIZES.wp('3%'),
    paddingHorizontal: SIZES.wp('5%'),
    paddingVertical: SIZES.wp('7%'),
  },
  dateDiv: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dotDiv: {
    width: SIZES.wp('1.5%'),
    height: SIZES.wp('1.5%'),
    backgroundColor: '#ADA9A2',
    borderRadius: SIZES.wp('13%'),
    marginRight: SIZES.wp('1%'),
  },
  iconDiv: {
    width: SIZES.wp('5%'),
    height: SIZES.wp('5%'),
    marginRight: SIZES.wp('3%'),
  },
  mintText: {
    fontFamily: FONTS.nunito_regular,
    fontSize: SIZES.wp('3.5%'),
    color: '#ADA9A2',
  },
  dailyText: {
    fontFamily: FONTS.nunito_regular,
    fontSize: SIZES.wp('3.5%'),
    color: '#1E1E1E',
  },
  mintDiv: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: SIZES.wp('3%'),
    marginBottom: SIZES.wp('7%'),
  },
  nameText: {
    fontFamily: FONTS.nunito_semibold,
    fontSize: SIZES.wp('4.3%'),
    color: '#000000',
  },
  button: {
    width: SIZES.wp('90%'),
    backgroundColor: '#8587DC',
    paddingVertical: SIZES.wp('4%'),
    borderRadius: SIZES.wp('7%'),
    alignItems: 'center',
    position: 'absolute',
    bottom: SIZES.wp('5%'),
    alignSelf: 'center',
  },
  nextText: {
    fontFamily: FONTS.nunito_medium,
    color: '#FDFBFF',
    fontSize: SIZES.wp('3.8%'),
  },
});
