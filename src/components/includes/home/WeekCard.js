import {
  StyleSheet,
  Text,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {FONTS} from '../../constants/index';
import {SIZES} from '../../constants/Constants';
import RightArrow from '../../../assets/icons/rightarrow.svg';

const WeekCard = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.scrollContain}>
        <TouchableOpacity
          onPress={() => navigation.navigate('WeeklyGoals')}
          style={styles.emojyContain}>
          <View style={styles.emojyDiv}></View>
          <Text style={styles.feelText}>Week - 1</Text>
        </TouchableOpacity>
        <View style={styles.emojyContain}>
          <View style={styles.emojyDiv}></View>
          <Text style={styles.feelText}>Week - 2</Text>
        </View>
        <View style={styles.emojyContain}>
          <View style={styles.emojyDiv}></View>
          <Text style={styles.feelText}>Week - 3</Text>
        </View>
        <View style={styles.emojyContain}>
          <View style={styles.emojyDiv}></View>
          <Text style={styles.feelText}>Week - 4</Text>
        </View>
      </View>
      <View style={styles.buildContain}>
        <Text style={styles.today}>Building Foundations</Text>
        <TouchableOpacity style={styles.completButton}>
          <Text style={styles.complete}>3 / 7 Completed</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.babyimageContain}>
        <View style={styles.babyimageDiv}></View>
      </View>
      <View style={styles.buildContain}>
        <Text style={styles.today}>Daily Goals</Text>
      </View>
      <View style={styles.DailyContainer}>
        <View style={styles.ButtonCom}>
          <Text style={styles.completeText}>Completed</Text>
        </View>
        <View style={styles.DailyDivLeft}></View>
        <View style={styles.DailyDivRight}>
          <Text style={styles.today}>Quiet Time Together:</Text>
          <View style={styles.DivRightText}>
            <Text style={styles.mind}>For Mind</Text>
            <TouchableOpacity
              onPress={() => navigation.navigate('DailyGoals')}
              style={styles.DivRightView}>
              <Text style={styles.today}>View</Text>
              <View style={styles.arrowRight}>
                <RightArrow width="100%" height="100%" />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default WeekCard;

const styles = StyleSheet.create({
  container: {
    width: SIZES.wp('90%'),
    padding: SIZES.wp('3%'),
    borderWidth: 1,
    borderColor: '#F4F4F4',
    marginTop: SIZES.wp('5%'),
    marginHorizontal: SIZES.wp('5%'),
    borderRadius: SIZES.wp('4%'),
  },
  buildContain: {
    borderLeftWidth: 2,
    borderColor: '#7E77DD',
    marginTop: SIZES.wp('4%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  today: {
    fontFamily: FONTS.nunito_semibold,
    color: '#000000',
    fontSize: SIZES.wp('4.3%'),
    marginLeft: SIZES.wp('3%'),
  },
  completButton: {
    backgroundColor: '#EEEDFF',
    paddingVertical: SIZES.wp('1%'),
    paddingHorizontal: SIZES.wp('1.5%'),
    borderRadius: SIZES.wp('1.8%'),
  },
  complete: {
    fontFamily: FONTS.nunito_regular,
    color: '#8587DC',
    fontSize: SIZES.wp('3.4%'),
  },
  babyimageContain: {
    borderWidth: 1,
    borderColor: '#F4F4F4',
    marginTop: SIZES.wp('4%'),
    backgroundColor: '#F9F9FF',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: SIZES.wp('4%'),
    borderRadius: SIZES.wp('3.5%'),
  },
  babyimageDiv: {
    width: SIZES.wp('49%'),
    height: SIZES.wp('35%'),
    borderWidth: 1,
  },
  DailyContainer: {
    borderWidth: 1,
    borderColor: '#F2FAFB',
    padding: SIZES.wp('4%'),
    marginTop: SIZES.wp('4%'),
    borderRadius: SIZES.wp('3.5%'),
    flexDirection: 'row',
  },
  ButtonCom: {
    backgroundColor: '#C8F1C4',
    position: 'absolute',
    top: 0,
    right: 0,
    paddingVertical: SIZES.wp('1%'),
    paddingHorizontal: SIZES.wp('3%'),
    borderBottomLeftRadius: SIZES.wp('2%'),
    borderTopRightRadius: SIZES.wp('2%'),
  },
  completeText: {
    fontFamily: FONTS.nunito_regular,
    color: '#6CAE67',
    fontSize: SIZES.wp('3.4%'),
  },
  DailyDivLeft: {
    width: SIZES.wp('18%'),
    height: SIZES.wp('18%'),
    borderWidth: 1,
    marginTop: SIZES.wp('2%'),
  },
  DailyDivRight: {
    justifyContent: 'center',
  },
  DivRightText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: SIZES.wp('3%'),
    marginTop: SIZES.wp('3%'),
  },
  DivRightView: {
    flexDirection: 'row',
    marginLeft: SIZES.wp('18%'),
    alignItems: 'center',
  },
  arrowRight: {
    width: SIZES.wp('4%'),
    height: SIZES.wp('4%'),
    marginLeft: SIZES.wp('2%'),
  },
  mind: {
    fontFamily: FONTS.nunito_regular,
    color: '#5E5E5E',
    fontSize: SIZES.wp('3.8%'),
  },
  scrollContain: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  emojyContain: {
    alignItems: 'center',
  },
  emojyDiv: {
    width: SIZES.wp('19%'),
    height: SIZES.wp('13%'),
    marginBottom: SIZES.wp('1.5%'),
    borderWidth: 1,
    borderRadius: SIZES.wp('2%'),
  },

  feelText: {
    fontFamily: FONTS.nunito_semibold,
    color: '#7E77DD',
    fontSize: SIZES.wp('3.5%'),
  },
});
