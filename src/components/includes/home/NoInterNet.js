import {
  StyleSheet,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {SIZES} from '../../constants/Constants';
import {FONTS} from '../../constants/index';

const NoInternet = () => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.div}>
        <Text style={styles.topText}>No Internet Connection</Text>
        <Text style={styles.bottomText}>Check Your Connection and refresh</Text>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.retryText}>Retry</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default NoInternet;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFDFB',
  },
  div: {
    alignItems: 'center',
  },
  topText: {
    fontFamily: FONTS.nunito_semibold,
    color: '#3E3E3E',
    fontSize: SIZES.wp('4.7%'),
    marginTop: SIZES.wp('5%'),
    marginBottom: SIZES.wp('2%'),
  },
  bottomText: {
    fontFamily: FONTS.nunito_regular,
    color: '#8E939C',
    fontSize: SIZES.wp('3.4%'),
    marginBottom: SIZES.wp('6%'),
  },
  refreshDiv: {
    width: SIZES.wp('5%'),
    height: SIZES.wp('5%'),
  },
  button: {
    backgroundColor: '#8587DC',
    flexDirection: 'row',
    paddingVertical: SIZES.wp('1.5%'),
    paddingHorizontal: SIZES.wp('5%'),
    borderRadius: SIZES.wp('8%'),
  },
  retryText: {
    fontFamily: FONTS.nunito_regular,
    color: '#FFFFFF',
    fontSize: SIZES.wp('3.6%'),
  },
});
