//Packeges
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export const COLORS = {
  green: '#459F56',
  bg: '#FFF',
  black: '#4A4A4A',
  grey: '#A2A0A0',
  greyTwo: '#C8C8C8',
  inputBorder: '#E7F1EE',
};

export const SIZES = {
  wp: wp,
  hp: hp,
};
